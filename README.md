使用方法是实例化BackUp类，然后调用all方法，传入文件名字即可。
在runtime下面的backup文件夹即可看到备份的数据库文件。
例子如下：

use leruge\BackUp;

$backUpFile = date('YmdHis') . '.sql';
$backUp = new BackUp();
$result = $backUp->all($backUpFile);

如果备份成功则返回数据如下
[
	'code' => 1,
	'message' => '备份完成！'
]
如果备份失败则直接抛出异常。

如果是自行开发项目，建议使用迁移类来创建数据库、表，然后备份结构和数据。
